//
//  MovingLoop.h
//  01_movingCam
//
//  Created by knekokz on 2015/02/28.
//
//

#ifndef ___1_movingCam__MovingLoop__
#define ___1_movingCam__MovingLoop__

#include <stdio.h>
#include "ofMain.h"

class MovingLoop{
public:
    void setup();
    void update();
    void draw();
    void changeRes();
    void changeSpeed();
    ofCamera cam;
    float radiusNoise;
    float camSpeed[3] = {200,800};
    float circleRes[3] = {6, 9, 64};
    int resNo;
    int speedNo;
private:
};


#endif /* defined(___1_movingCam__MovingLoop__) */
