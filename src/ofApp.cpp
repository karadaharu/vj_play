#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    loop.setup();
    model.setup();
    checker.setup();
    for(int i =0; i < 4; i++){
        sceneSwitch[i] = false;
    }
    
    //
    this->soundStream.listDevices();
    soundStream.setDeviceID(0);
    soundStream.setInput(0);
    this->soundStream.setup(this, 0, 1, 44100, BUFFER_SIZE, 4);
    this->_left  = new float[BUFFER_SIZE];
    this->_right = new float[BUFFER_SIZE];
    
    for(int i = 0; i < NUM_WINDOWS; i++){
        for(int j = 0; j < BUFFER_SIZE / 2; j++){
            this->_freq[i][j] = 0.0f;
        }
    }

    
    for(int i = 0; i < 3; i++){
        timer[i] = 0;
    }
    
    //checker
    img_id = 0;
    
    myFbo.allocate(1024,768);
    myGlitch.setup(&myFbo, "Shaders");
    myGlitch.listShaders();
    
    for(int i = 0; i < 10; i++){
        glitchSw[i] = true;
    }
}

//--------------------------------------------------------------
void ofApp::update(){
    if(sceneSwitch[0]){
        loop.update();
    }
    if(sceneSwitch[1]){
        model.update();
    }
    
    //timer
    if(ofGetElapsedTimef() - timer[0] > 5){
        int on_sw = (int)ofRandom(3);
        if(sceneSwitch[0]==false && sceneSwitch[2]==false){
            sceneSwitch[2]= true;
            sceneSwitch[0]= true;            
        }
        timer[0] = ofGetElapsedTimef();
        int sw = (int)ofRandom(16);
        if(sw==1){
            sceneSwitch[0] = !sceneSwitch[0];
        }else if(sw==2){
            sceneSwitch[1] = !sceneSwitch[1];
        }else if(sw==3){
            sceneSwitch[2] = !sceneSwitch[2];
        }else if(sw==4){
            loop.changeRes();
        }else if(sw==5){
            loop.changeRes();
        }else if(sw==6){
            model.changeModel();
        }else if(sw==7){
            checker.changeRecursion();
        }else if(sw==8){
            checker.is_img = !checker.is_img;
        }else if(sw==9){
            loop.changeRes();
        }else if(sw==10){
            loop.changeRes();

        }else if(sw==11){
            model.changeModel();
        }else if(sw==12){
             checker.changeRecursion();
        }else if(sw==13){
            checker.is_img = !checker.is_img;
        }else if(sw==14){
            checker.is_img = !checker.is_img;
        }else if(sw==15){
            img_id++;
            if(img_id==2){
                img_id=0;
            }
            checker.changeImage(img_name[img_id]);
        }
        
        
        sw = (int)ofRandom(12);
        if (sw == 1){
            myGlitch.setFx(OFXPOSTGLITCH_CONVERGENCE	, glitchSw[sw-1]);
        }
        if (sw == 2){
            myGlitch.setFx(OFXPOSTGLITCH_GLOW			,  glitchSw[sw-1]);
        }
        if (sw == 3){
            myGlitch.setFx(OFXPOSTGLITCH_SHAKER			, true);
            sceneSwitch[2] = !sceneSwitch[2];
        }
        if (sw == 4){
            myGlitch.setFx(OFXPOSTGLITCH_CUTSLIDER		, glitchSw[sw-1]);
            
        }        if (sw == 5) myGlitch.setFx(OFXPOSTGLITCH_TWIST			, glitchSw[sw-1]);
        if (sw == 6) myGlitch.setFx(OFXPOSTGLITCH_OUTLINE		, glitchSw[sw-1]);
        if (sw == 7) myGlitch.setFx(OFXPOSTGLITCH_NOISE			, glitchSw[sw-1]);
        if (sw == 8) myGlitch.setFx(OFXPOSTGLITCH_SLITSCAN		, glitchSw[sw-1]);
        if (sw == 9) myGlitch.setFx(OFXPOSTGLITCH_SWELL			, glitchSw[sw-1]);
        if (sw == 10) myGlitch.setFx(OFXPOSTGLITCH_INVERT			, glitchSw[sw-1]);
        glitchSw[sw-1] = !glitchSw[sw-1];
    }
    
    
    
    //////////////////
//
    float avg_power = 0.0f;
    // 左チャンネルのデータに対してのみFFTを実行
    this->_fft.PowerSpectrum(0, (int)BUFFER_SIZE/2,
                             this->_left, BUFFER_SIZE, &this->_magnitude[0], &this->_phase[0], &this->_power[0], &avg_power);
    // _magnitude[0]は直流成分(時間変化のない=振動しない成分)のため、無視する
    float width = (float)ofGetWidth() / (float)(BUFFER_SIZE/2 - 1);
    for(int i = 1; i < (int)(BUFFER_SIZE/2); i++){
        ofRect((i - 1)*width, ofGetHeight(), width, -(this->_magnitude[i] * 200.0));
    }
    
    myFbo.begin();
    ofBackground(47, 205, 180);
    if(sceneSwitch[1]){
        model.draw();
    }
    if(sceneSwitch[0]){
        loop.draw();
    }
    if(sceneSwitch[2]){
        checker.size = -20 + curVol * 400;
        checker.draw();
    }
    myFbo.end();
    // FX level is a multiperpose value
    myGlitch.setFxLevel("original", ofMap(ofGetMouseX(), 0, ofGetWidth(), 0.0, 0.1, true));
}

//--------------------------------------------------------------
void ofApp::draw(){
//    float avg_power = 0.0f;
//    // 左チャンネルのデータに対してのみFFTを実行
//    this->_fft.PowerSpectrum(0, (int)BUFFER_SIZE/2,
//                             this->_left, BUFFER_SIZE, &this->_magnitude[0], &this->_phase[0], &this->_power[0], &avg_power);
//    // _magnitude[0]は直流成分(時間変化のない=振動しない成分)のため、無視する
//    float width = (float)ofGetWidth() / (float)(BUFFER_SIZE/2 - 1);
//    for(int i = 1; i < (int)(BUFFER_SIZE/2); i++){
//        ofRect((i - 1)*width, ofGetHeight(), width, -(this->_magnitude[i] * 200.0));
//    }
//    
//    
//    ofBackground(47, 205, 180);
//    if(sceneSwitch[1]){
//        model.draw();
//    }
//    
//    if(sceneSwitch[0]){
//        loop.draw();
//    }
//    if(sceneSwitch[2]){
//        checker.size = -20 + curVol * 400;
//        checker.draw();
//    }

    ofPushStyle();
    ofSetRectMode(OF_RECTMODE_CORNER);
    myGlitch.generateFx();
    myFbo.draw(0,0,ofGetWidth(), ofGetHeight());
    ofPopStyle();

}

void ofApp::audioIn(float * input, int bufferSize, int nChannels){
    curVol = 0.0;
    for (int i = 0; i < bufferSize; i++){
        curVol += input[i*2+1]*input[i*2+1]; // 聴覚上は出力の2乗に比例するので、2乗しています。
        this->_left[i]  = input[i * 2];
        //        this->_right[i] = input[i * 2 + 1]; //右をどこにも使ってない
    }
    curVol /= bufferSize; // 平均を取ります。
}


//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if(key=='f'){
        ofToggleFullscreen();
    }else if(key=='1'){
        sceneSwitch[0] = !sceneSwitch[0];
    }else if(key=='2'){
        sceneSwitch[1] = !sceneSwitch[1];
    }else if(key=='q'){
        loop.changeRes();
    }else if(key=='a'){
        loop.changeSpeed();
    }else if(key=='w'){
        model.changeModel();
    }
    else if(key=='e'){
        checker.changeRecursion();
    }else if(key=='d'){
        checker.is_img = !checker.is_img;
    }else if(key=='c'){
        img_id++;
        if(img_id==2){
            img_id=0;
        }
        checker.changeImage(img_name[img_id]);
    }
    if (key == '1') myGlitch.setFx(OFXPOSTGLITCH_CONVERGENCE	, true);
    if (key == '2') myGlitch.setFx(OFXPOSTGLITCH_GLOW			, true);
    if (key == '3'){
        myGlitch.setFx(OFXPOSTGLITCH_SHAKER			, true);
        sceneSwitch[2] = !sceneSwitch[2];
    }
    if (key == '4') myGlitch.setFx(OFXPOSTGLITCH_CUTSLIDER		, true);
    if (key == '5') myGlitch.setFx(OFXPOSTGLITCH_TWIST			, true);
    if (key == '6') myGlitch.setFx(OFXPOSTGLITCH_OUTLINE		, true);
    if (key == '7') myGlitch.setFx(OFXPOSTGLITCH_NOISE			, true);
    if (key == '8') myGlitch.setFx(OFXPOSTGLITCH_SLITSCAN		, true);
    if (key == '9') myGlitch.setFx(OFXPOSTGLITCH_SWELL			, true);
    if (key == '0') myGlitch.setFx(OFXPOSTGLITCH_INVERT			, true);
    
    if (key == 'q') myGlitch.setFx(OFXPOSTGLITCH_CR_HIGHCONTRAST, true);
    if (key == 'w') myGlitch.setFx(OFXPOSTGLITCH_CR_BLUERAISE	, true);
    if (key == 'e') myGlitch.setFx(OFXPOSTGLITCH_CR_REDRAISE	, true);
    if (key == 'r') myGlitch.setFx(OFXPOSTGLITCH_CR_GREENRAISE	, true);
    if (key == 't') myGlitch.setFx(OFXPOSTGLITCH_CR_BLUEINVERT	, true);
    if (key == 'y') myGlitch.setFx(OFXPOSTGLITCH_CR_REDINVERT	, true);
    if (key == 'u') myGlitch.setFx(OFXPOSTGLITCH_CR_GREENINVERT	, true);
    
    if (key == 'a') myGlitch.setFx("original"                   , true);
    
    if (key == 's') myGlitch.setFxTo(OFXPOSTGLITCH_CONVERGENCE  , 1);

    
    if (key == 'c') myGlitch.setShaders("Shaders");
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    if (key == '1') myGlitch.setFx(OFXPOSTGLITCH_CONVERGENCE	, false);
    if (key == '2') myGlitch.setFx(OFXPOSTGLITCH_GLOW			, false);
    if (key == '3') myGlitch.setFx(OFXPOSTGLITCH_SHAKER			, false);
    if (key == '4') myGlitch.setFx(OFXPOSTGLITCH_CUTSLIDER		, false);
    if (key == '5') myGlitch.setFx(OFXPOSTGLITCH_TWIST			, false);
    if (key == '6') myGlitch.setFx(OFXPOSTGLITCH_OUTLINE		, false);
    if (key == '7') myGlitch.setFx(OFXPOSTGLITCH_NOISE			, false);
    if (key == '8') myGlitch.setFx(OFXPOSTGLITCH_SLITSCAN		, false);
    if (key == '9') myGlitch.setFx(OFXPOSTGLITCH_SWELL			, false);
    if (key == '0') myGlitch.setFx(OFXPOSTGLITCH_INVERT			, false);
    if (key == 'q') myGlitch.setFx(OFXPOSTGLITCH_CR_HIGHCONTRAST, false);
    if (key == 'w') myGlitch.setFx(OFXPOSTGLITCH_CR_BLUERAISE	, false);
    if (key == 'e') myGlitch.setFx(OFXPOSTGLITCH_CR_REDRAISE	, false);
    if (key == 'r') myGlitch.setFx(OFXPOSTGLITCH_CR_GREENRAISE	, false);
    if (key == 't') myGlitch.setFx(OFXPOSTGLITCH_CR_BLUEINVERT	, false);
    if (key == 'y') myGlitch.setFx(OFXPOSTGLITCH_CR_REDINVERT	, false);
    if (key == 'u') myGlitch.setFx(OFXPOSTGLITCH_CR_GREENINVERT	, false);
    
    if (key == 'a') myGlitch.setFx("original"                   , false);
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
