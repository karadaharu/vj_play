//
//  Checker.h
//  03_checker
//
//  Created by knekokz on 2015/02/25.
//
//

#ifndef ___3_checker__Checker__
#define ___3_checker__Checker__

#include <stdio.h>
#include "ofMain.h"

class Checker {
public:
    void setup();
    void draw();
    void changeRecursion();
    void changeImage(string filename);
    int n_recursion;
    ofImage img;
    bool is_img;
    float size;
    float img_ratio;
};




#endif /* defined(___3_checker__Checker__) */
