//
//  Model3D.h
//  02_3D
//
//  Created by knekokz on 2015/02/25.
//
//

#ifndef ___2_3D__Model3D__
#define ___2_3D__Model3D__

#include <stdio.h>
#include "ofx3DModelLoader.h"

class Model3D {
public:
    void setup();
    void update();
    void draw();
    ofx3DModelLoader model;
    void changeModel();
    int modelNo;
private:
};


#endif /* defined(___2_3D__Model3D__) */
