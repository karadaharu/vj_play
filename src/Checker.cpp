//
//  Checker.cpp
//  03_checker
//
//  Created by knekokz on 2015/02/25.
//
//

#include "Checker.h"

void Checker::setup(){
    ofSetRectMode(OF_RECTMODE_CENTER);
    img.loadImage("./kaomoji.png");
    is_img = true;
    n_recursion = 0;
    size = 0;
    img_ratio = (float)img.height/(float)img.width;
    cout <<"w:"<< img.width << "h:" << img.height<<"ratio:"<<img_ratio;
}

void Checker::changeImage(string filename){
    img.loadImage(filename);
    img_ratio = (float)img.height/(float)img.width;    
}

void Checker::draw(){
//    ofPushMatrix();
//    ofTranslate(-1*ofGetWidth()/2,-1*ofGetHeight()/2);
    
    float w_global = ofGetWidth();
    float h_global = ofGetHeight();
    
    int n_grid_line = pow(2.f, n_recursion);
    
    float w_cell = w_global / pow(2.f, n_recursion);
    float h_cell = h_global / pow(2.f, n_recursion);
    
    for(int n_w = 0; n_w < n_grid_line; n_w++){
        for(int n_h = 0; n_h < n_grid_line; n_h++){
            ofPushMatrix();
            ofTranslate( n_w * w_cell + w_cell / 2 , n_h * h_cell + h_cell / 2);
            if(is_img){
                img.draw(0, 0, w_cell/2 + size, img_ratio * (w_cell/2 + size));
            }else{
                ofFill();                
                ofRect(0, 0, w_cell/2 + size, w_cell/2 + size);
            }
            ofPopMatrix();
        }
    }
//    ofPopMatrix();
}

void Checker::changeRecursion(){
    if(n_recursion<5){
        n_recursion++;
    }else{
        n_recursion = 0;
    }
}