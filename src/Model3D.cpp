//
//  Model3D.cpp
//  02_3D
//
//  Created by knekokz on 2015/02/25.
//
//

#include "Model3D.h"

// クロスプラットフォームにするための専用rgba配列
GLfloat lightOnePosition[] = {40.0, 40, 100.0, 0.0};
GLfloat lightOneColor[] = {0, 0, 0.99, 0.5};

GLfloat lightTwoPosition[] = {-40.0, 40, 100.0, 0.0};
GLfloat lightTwoColor[] = {0.99, 0.99, 0.99, 1.0};

//GLfloat lightThreePosition[] = {500, 300, 500.0, 1};
//GLfloat lightThreeColor[] = {0, 0, 0.99, 0.5};
//GLfloat lightThreeDirction[] = {-1.0,-1.0,0};
//GLfloat lightThreeSpecular[] = {0.99,0.0,0.0,0.5};

void Model3D::setup(){
    //some model / light stuff
    ofEnableDepthTest();
    glShadeModel (GL_SMOOTH);
    
    /* initialize lighting */
    // glLigt:光についてのプロパティ
    // GL_LITGHTi:光源id 0~最低8まで
    // 光源があたっていないところが真っ暗になるので2つ光源をセットする
    glLightfv (GL_LIGHT0, GL_POSITION, lightOnePosition);
    glLightfv (GL_LIGHT0, GL_DIFFUSE, lightOneColor);
    glEnable (GL_LIGHT0);
    
    glLightfv (GL_LIGHT1, GL_POSITION, lightTwoPosition);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, lightTwoColor);
    glEnable (GL_LIGHT1);
    
//    glLightfv (GL_LIGHT2, GL_POSITION, lightThreePosition);
//    glLightfv (GL_LIGHT2, GL_DIFFUSE, lightThreeColor);
//    glLightfv (GL_LIGHT2, GL_SPECULAR, lightThreeColor);
//    glLightfv (GL_LIGHT2, GL_SPOT_DIRECTION, lightThreeDirction);
//    glEnable (GL_LIGHT2);
    
    glEnable (GL_LIGHTING);
    glColorMaterial (GL_FRONT_AND_BACK, GL_DIFFUSE);
    glEnable (GL_COLOR_MATERIAL);
    
    //load the female model - the 3ds and the texture file need to be in the same folder
    model.loadModel("female2/pose-dance2.3ds");
    
    //you can create as many rotations as you want
    //choose which axis you want it to effect
    //you can update these rotations later on
    model.setRotation(0, 90, 1, 0, 0);
    model.setRotation(1, 270, 0, 0, 1);
    model.setScale(700, 700, 700);

    modelNo = 0;
}

void Model3D::update(){
    model.setPosition(ofGetWidth()/2, ofGetHeight()/2+200, -100);    
    model.setRotation(1, 270 + ofGetElapsedTimef() * 60, 0, 0, 1);
}

void Model3D::draw(){
    ofBackground(184, 117, 255);
    //lets tumble the world with the mouse
    glPushMatrix();
    //draw in middle of the screen
    glTranslatef(ofGetWidth()/2,ofGetHeight()/2,0);
    //tumble according to mouse
    glTranslatef(-ofGetWidth()/2,-ofGetHeight()/2,0);
//    ofSetColor(0, 0, 255, 250);
    model.draw();
//    ofSetColor(0, 0, 0, 255);
    glPopMatrix();
}

void Model3D::changeModel(){
    if(modelNo < 1){
        modelNo++;
    }else{
        modelNo = 0;
    }
    
    if(modelNo==0){
        model.loadModel("female2/pose-dance2.3ds");
        model.setRotation(0, 90, 1, 0, 0);
        model.setRotation(1, 270, 0, 0, 1);
        model.setScale(700, 700, 700);
    }else if(modelNo==1){
        model.loadModel("male2/model.3DS");
//        model.setRotation(0, 90, 1, 0, 0);
        model.setRotation(0, 270, 0, 0, 1);
        model.setRotation(1, 270, 1, 0, 1);
        
        model.setScale(400, 400, 400);
    }
}