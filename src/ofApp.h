#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "MovingLoop.h"
#include "Model3D.h"
#include "Checker.h"
#include "fft.h"
#include "ofxPostGlitch.h"

#define BUFFER_SIZE 256
#define NUM_WINDOWS 80

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    MovingLoop loop;
    Model3D model;

    bool sceneSwitch[4];
    
    // FFT関連
    void audioIn(float * input, int bufferSize, int nChannels);
    float *_left;
    float *_right;
    fft    _fft;
    float  _magnitude[BUFFER_SIZE];
    float  _phase[BUFFER_SIZE];
    float  _power[BUFFER_SIZE];
    float  _freq[NUM_WINDOWS][BUFFER_SIZE/2];
    ofSoundStream soundStream;
    float curVol;
    
    //タイマー
    int timer[3];
    
    // checker
    Checker checker;
    string img_name[2] = {"kaomoji.png", "kanji.png"};
    int img_id;
    
    ofxPostGlitch	myGlitch;
    ofFbo			myFbo;
	ofTexture		texture;
    bool glitchSw[10];
};
