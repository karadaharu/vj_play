//
//  MovingLoop.cpp
//  01_movingCam
//
//  Created by knekokz on 2015/02/28.
//
//

#include "MovingLoop.h"

void MovingLoop::setup(){
    speedNo = 1;
    resNo = 0;
    radiusNoise = ofRandom(10);
}

void MovingLoop::update(){
    ofSetCircleResolution(circleRes[resNo]);
    cam.lookAt(ofVec3f(0,0,0));
    cam.setPosition(0, 0, -1* camSpeed[speedNo] *ofGetElapsedTimef());
    ofVec3f camPosition = cam.getPosition();
    ofVec3f camLookat = camPosition;
    camLookat.z = camLookat.z - 100;
    cam.lookAt(camLookat);
    radiusNoise += 0.1;
}

void MovingLoop::draw(){
    cam.begin();
    ofNoFill();
    ofVec3f camPosition = cam.getPosition();
    float zStep = 10;
    int step = camPosition.z / zStep;
    for(int i = 0; i < 100; i++){
        float zCircle = -zStep*i + zStep * step;
        ofRotateZ(zCircle/100);
        ofCircle(0, 0, zCircle, 50+ofNoise(zCircle/200)*50);
    }
    cam.end();
}

void MovingLoop::changeRes(){
    if(resNo<2){
        resNo++;
    }else{
        resNo = 0;
    }
}

void MovingLoop::changeSpeed(){
    if(speedNo<1){
        speedNo++;
    }else{
        speedNo = 0;
    }
}